/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.skills.l2skills;

import net.sf.l2j.gameserver.model.L2Character;
import net.sf.l2j.gameserver.model.L2ItemInstance;
import net.sf.l2j.gameserver.model.L2Object;
import net.sf.l2j.gameserver.model.L2Skill;
import net.sf.l2j.gameserver.model.actor.instance.L2PcInstance;
import net.sf.l2j.gameserver.network.serverpackets.SystemMessage;
import net.sf.l2j.gameserver.skills.BaseStats;
import net.sf.l2j.gameserver.skills.Formulas;
import net.sf.l2j.gameserver.templates.StatsSet;

public class L2SkillChargeDmg extends L2Skill
{
	final int num_charges;
	
	public L2SkillChargeDmg(StatsSet set)
	{
		super(set);
		num_charges = set.getInteger("num_charges", getLevel());
	}
	
	@Override
	public boolean checkCondition(L2Character activeChar, L2Object target, boolean itemOrWeapon)
	{
		L2PcInstance player = (L2PcInstance) activeChar;
		
		if (player.getCharges() < num_charges)
		{
			SystemMessage sm = new SystemMessage(113);
			sm.addSkillName(this);
			activeChar.sendPacket(sm);
			return false;
		}
		return super.checkCondition(activeChar, target, itemOrWeapon);
	}
	
	/**
	 * (non-Javadoc)
	 * @see net.sf.l2j.gameserver.model.L2Skill#isCritical(L2Character, L2Character)
	 */
	@Override
	public boolean isCritical(L2Character caster, L2Character target)
	{
		return Formulas.getInstance().calcCrit(getBaseCritRate() * 10 * BaseStats.STR.calcBonus(caster));
	}
	
	@Override
	public void useSkill(L2Character caster, L2Object[] targets, boolean isFirstCritical)
	{
		L2PcInstance player = (L2PcInstance) caster;
		if (caster.isAlikeDead())
		{
			return;
		}
		
		// Formula tested by L2Guru
		double modifier = 0;
		modifier = 0.8 + (0.201 * player.getCharges());
		
		// Consume charges
		player.decreaseCharges(num_charges);
		
		L2ItemInstance weapon = caster.getActiveWeaponInstance();
		boolean soul = (weapon != null && weapon.getChargedSoulShot() == L2ItemInstance.CHARGED_SOULSHOT);
		
		for (int i = 0; i < targets.length; i++)
		{
			L2Object trg = targets[i];
			if (!(trg instanceof L2Character))
			{
				continue;
			}
			L2Character target = (L2Character) trg;
			
			boolean shld = Formulas.getInstance().calcShldUse(caster, target);
			boolean crit = false;
			if (getBaseCritRate() > 0)
			{
				crit = i == 0 ? isFirstCritical : isCritical(caster, target);
			}
			
			double damage = Formulas.getInstance().calcPhysDam(caster, target, this, shld, false, false, soul);
			
			if (!crit && (getCondition() & COND_CRIT) != 0)
			{
				damage = 0;
			}
			
			if (crit)
			{
				damage *= 2;
			}
			
			if (damage > 0)
			{
				damage = damage * modifier;
				caster.sendDamageMessage(target, (int) damage, false, false, false);
				target.reduceCurrentHp(damage, caster);
			}
		}
	}
}
