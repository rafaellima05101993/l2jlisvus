/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.model.olympiad;

import java.util.List;
import java.util.logging.Logger;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.datatables.SkillTable;
import net.sf.l2j.gameserver.model.L2ItemInstance;
import net.sf.l2j.gameserver.model.L2Party;
import net.sf.l2j.gameserver.model.L2Skill;
import net.sf.l2j.gameserver.model.L2Summon;
import net.sf.l2j.gameserver.model.L2World;
import net.sf.l2j.gameserver.model.actor.instance.L2CubicInstance;
import net.sf.l2j.gameserver.model.actor.instance.L2PcInstance;
import net.sf.l2j.gameserver.model.actor.instance.L2PetInstance;
import net.sf.l2j.gameserver.model.itemcontainer.Inventory;
import net.sf.l2j.gameserver.model.olympiad.Olympiad.COMP_TYPE;
import net.sf.l2j.gameserver.network.serverpackets.ExOlympiadMatchEnd;
import net.sf.l2j.gameserver.network.serverpackets.ExOlympiadMode;
import net.sf.l2j.gameserver.network.serverpackets.ExOlympiadUserInfo;
import net.sf.l2j.gameserver.network.serverpackets.InventoryUpdate;
import net.sf.l2j.gameserver.network.serverpackets.SystemMessage;
import net.sf.l2j.gameserver.templates.StatsSet;

/**
 * @author DnR
 *
 */
class OlympiadGame
{
	protected static final Logger _log = Logger.getLogger(OlympiadGame.class.getName());
	
    protected COMP_TYPE _type;
    protected boolean _aborted;
    protected boolean _playerOneDisconnected;
    protected boolean _playerTwoDisconnected;
    protected String _playerOneName;
    protected String _playerTwoName;
    protected int _playerOneID = 0;
    protected int _playerTwoID = 0;

    protected static boolean _battleStarted;
    private static final String POINTS = "olympiad_points";
	private static final String COMP_DONE = "competitions_done";
	private static final String COMP_WON = "competitions_won";
	private static final String COMP_LOST = "competitions_lost";
	
	// Game state
	private byte _state;
	protected static byte INITIAL = 0;
    protected static byte STANDBY = 1;
    protected static byte PLAYING = 2;
    
    protected L2PcInstance _playerOne;
    protected L2PcInstance _playerTwo;
    private final int _stadiumID;
    private List<L2PcInstance> _players;
    private int x1, y1, z1, x2, y2, z2;
    private int[] _stadiumPort;
    private SystemMessage _sm;
    private SystemMessage _sm2;
    private SystemMessage _sm3;

	protected OlympiadGame(int id, COMP_TYPE type, List<L2PcInstance> list, int[] stadiumPort)
	{
		_stadiumID = id;
        _aborted = false;
        _state = INITIAL;
        _playerOneDisconnected = false;
        _playerTwoDisconnected = false;
        _type = type;
        _stadiumPort = stadiumPort;

        if (list != null)
        {
            _players = list;
            _playerOne = list.get(0);
            _playerTwo = list.get(1);

            try
            {
                _playerOneName = _playerOne.getName();
                _playerTwoName = _playerTwo.getName();
                _playerOne.setOlympiadGameId(id);
                _playerTwo.setOlympiadGameId(id);
                _playerOneID = _playerOne.getObjectId();
                _playerTwoID = _playerTwo.getObjectId();
            }
            catch (Exception e)
            {
                _aborted = true;
                clearPlayers();
            }

            if (Config.DEBUG)
                _log.info("Olympiad System: Game - " + id + ": " + _playerOne.getName() + " Vs " + _playerTwo.getName());
        }
        else
        {
            _aborted = true;
            clearPlayers();
            return;
        }
    }

    protected void clearPlayers()
    {
        _playerOne = null;
        _playerTwo = null;
        _players = null;
        _playerOneName = "";
        _playerTwoName = "";
        _playerOneID = 0;
        _playerTwoID = 0;
    }

    protected void handleDisconnect(L2PcInstance player)
    {
        if (player == _playerOne)
            _playerOneDisconnected = true;
        else if (player == _playerTwo)
            _playerTwoDisconnected = true;
        
        // Also, reset player position if he is already inside arena
        if (player.isInOlympiadMode())
        {
        	setPlayerXYZ(player);
        }
    }
    
    protected void setPlayerXYZ(L2PcInstance player)
    {
    	int x, y, z = 0;
    	if (player == _playerOne)
    	{
    		x = x1;
    		y = y1;
    		z = z1;
    	}
    	else if (player == _playerTwo)
    	{
    		x = x2;
    		y = y2;
    		z = z2;
    	}
    	else
    	{
    		return; // Invalid character
    	}

    	player.setXYZ(x, y, z);
    }

    protected void removals()
	{
        if (_aborted)
            return;

        if (_playerOne == null || _playerTwo == null)
            return;

        if (_playerOneDisconnected || _playerTwoDisconnected)
            return;

        for (L2PcInstance player : _players)
        {
            try
            {
                // Abort casting if player casting  
                if (player.isCastingNow())
                    player.abortCast();

                player.getAppearance().setVisible();

                // Heal Player fully
                player.setCurrentCp(player.getMaxCp());
                player.setCurrentHp(player.getMaxHp());
                player.setCurrentMp(player.getMaxMp());

                // Remove Buffs
                player.stopAllEffects();
                player.clearCharges();

                // Remove Summon's Buffs
                if (player.getPet() != null)
                {
                    L2Summon summon = player.getPet();
                    summon.stopAllEffects();

                    if (summon.isCastingNow())
                        summon.abortCast();

                    if (summon instanceof L2PetInstance)
                        summon.unSummon(player);
                }

                if (player.getCubics() != null)
                {
                    boolean removed = false;
                    for (L2CubicInstance cubic : player.getCubics().values())
                    {
                        if (cubic.givenByOther())
                        {
                            cubic.stopAction();
                            cubic.cancelDisappear();
                            player.delCubic(cubic.getId());
                            removed = true;
                        }
                    }

                    if (removed)
                        player.broadcastUserInfo();
                }

                // Remove player from his party
                if (player.getParty() != null)
                {
                    L2Party party = player.getParty();
                    party.removePartyMember(player, true);
                }

                // Remove Hero Weapons
                // check to prevent the using of weapon/shield on strider/wyvern
                L2ItemInstance wpn = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
                if (wpn == null)
                    wpn = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LRHAND);

                if (wpn != null && wpn.isHeroItem())
                {
                    L2ItemInstance[] unequiped = player.getInventory().unEquipItemInBodySlotAndRecord(wpn.getItem().getBodyPart());
                    InventoryUpdate iu = new InventoryUpdate();
                    for (L2ItemInstance element : unequiped)
                    {
                    	iu.addModifiedItem(element);
                    }
                    player.sendPacket(iu);
                    player.abortAttack();
                    player.broadcastUserInfo();

                    // this can be 0 if the user pressed the right mouse button twice very fast
                    if (unequiped.length > 0)
                    {
                        if (unequiped[0].isWear())
                            return;

                        SystemMessage sm = null;
                        if (unequiped[0].getEnchantLevel() > 0)
                        {
                            sm = new SystemMessage(SystemMessage.EQUIPMENT_S1_S2_REMOVED);
                            sm.addNumber(unequiped[0].getEnchantLevel());
                            sm.addItemName(unequiped[0].getItemId());
                        }
                        else
                        {
                            sm = new SystemMessage(SystemMessage.S1_DISARMED);
                            sm.addItemName(unequiped[0].getItemId());
                        }
                        player.sendPacket(sm);
                    }
                }

                // Remove forbidden items
                player.checkItemRestriction();
                
                // Remove shot automation
                player.disableAutoShotsAll();

                // Discharge any active shots
                player.getActiveWeaponInstance().setChargedSoulShot(L2ItemInstance.CHARGED_NONE);
                player.getActiveWeaponInstance().setChargedSpiritShot(L2ItemInstance.CHARGED_NONE);
                
                // Same goes for pets
                L2Summon summon = player.getPet();
                if (summon != null)
                {
                	summon.setChargedSoulShot(L2ItemInstance.CHARGED_NONE);
                	summon.setChargedSpiritShot(L2ItemInstance.CHARGED_NONE);
                }
            }
            catch (Exception e) {}
        }
    }

    protected boolean portPlayersToArena()
    {    		
        boolean _playerOneCrash = (_playerOne == null || _playerOneDisconnected);
        boolean _playerTwoCrash = (_playerTwo == null || _playerTwoDisconnected);

        if (_playerOneCrash || _playerTwoCrash || _aborted)
        {
            _playerOne = null;
            _playerTwo = null;
            _aborted = true;
            return false;
        }

        try
        { 
            x1 = _playerOne.getX();
            y1 = _playerOne.getY();
            z1 = _playerOne.getZ();

            x2 = _playerTwo.getX();
            y2 = _playerTwo.getY();
            z2 = _playerTwo.getZ();


            if (_playerOne.isSitting())
                _playerOne.standUp();

            if (_playerTwo.isSitting())
                _playerTwo.standUp();

            _playerOne.setTarget(null);
            _playerTwo.setTarget(null);

            _playerOne.teleToLocation(_stadiumPort[0], _stadiumPort[1], _stadiumPort[2], true);
            _playerTwo.teleToLocation(_stadiumPort[0], _stadiumPort[1], _stadiumPort[2], true);

            _playerOne.setIsInOlympiadMode(true);
            _playerOne.setIsOlympiadStart(false);
            _playerOne.setOlympiadSide(1);

            _playerTwo.setIsInOlympiadMode(true);
            _playerTwo.setIsOlympiadStart(false);
            _playerTwo.setOlympiadSide(2);

            _playerOne.sendPacket(new ExOlympiadMode(1));
            _playerTwo.sendPacket(new ExOlympiadMode(2));

            _state = STANDBY;
        }
        catch (NullPointerException e)
        { 
            return false; 
        }
        return true;
    }

    protected void sendMessageToPlayers(boolean toBattleBegin, int nsecond)
    {
        if (!toBattleBegin)
            _sm = new SystemMessage(SystemMessage.YOU_WILL_ENTER_THE_OLYMPIAD_STADIUM_IN_S1_SECOND_S);
        else
            _sm = new SystemMessage(SystemMessage.THE_GAME_WILL_START_IN_S1_SECOND_S);

        _sm.addNumber(nsecond);

        for (L2PcInstance player : _players)
        {
            if (player != null)
                player.sendPacket(_sm);
        }
    }

    protected void broadcastMessage(SystemMessage sm, boolean toAll)
    {
        for (L2PcInstance player : _players)
        {
            if (player != null)
                player.sendPacket(sm);
        }

        List<L2PcInstance> spectators = OlympiadManager.STADIUMS[_stadiumID].getSpectators();
        if (toAll && spectators != null)
        {
            for (L2PcInstance spec : spectators)
            {
                if (spec == null)
                    continue;

                spec.sendPacket(sm);
            }
        }
    }

    protected void portPlayersBack()
    {
        if (_playerOne != null)
        {
            _playerOne.sendPacket(new ExOlympiadMatchEnd());
            _playerOne.teleToLocation(x1, y1, z1, true);
        }

        if (_playerTwo != null)
        {
            _playerTwo.sendPacket(new ExOlympiadMatchEnd());
            _playerTwo.teleToLocation(x2, y2, z2, true);
        }
    }

    protected void playersStatusBack()
    {
        for (L2PcInstance player : _players)
        {
            try
            {
                if (player.isDead())
                    player.setIsDead(false);

                player.getStatus().startHpMpRegeneration();
                player.setCurrentCp(player.getMaxCp());
                player.setCurrentHp(player.getMaxHp());
                player.setCurrentMp(player.getMaxMp());
                player.setIsInOlympiadMode(false);
                player.setIsOlympiadStart(false);
                player.setOlympiadSide(-1);
                player.setOlympiadGameId(-1);
                player.sendPacket(new ExOlympiadMode(0));
            }
            catch (Exception e) {}
        }
    }
    
    protected boolean checkBattleStatus()
    {
        boolean _pOneCrash = (_playerOne == null || _playerOneDisconnected);
        boolean _pTwoCrash = (_playerTwo == null || _playerTwoDisconnected);

        int _div;
        switch (_type)
		{
			case NON_CLASSED:
				_div = 5;
				break;
			default:
				_div = 3;
				break;
		}
        
        if (_pOneCrash || _pTwoCrash || _aborted)
        {
            StatsSet playerOneStat = Olympiad.getNobleStats(_playerOneID);
            StatsSet playerTwoStat = Olympiad.getNobleStats(_playerTwoID);

            int playerOnePlayed = playerOneStat.getInteger(COMP_DONE);
            int playerTwoPlayed = playerTwoStat.getInteger(COMP_DONE);
            int playerOneWon = playerOneStat.getInteger(COMP_WON);
            int playerTwoWon = playerTwoStat.getInteger(COMP_WON);
            int playerOneLost = playerOneStat.getInteger(COMP_LOST);
            int playerTwoLost = playerTwoStat.getInteger(COMP_LOST);
            
            int playerOnePoints = playerOneStat.getInteger(POINTS);
            int playerTwoPoints = playerTwoStat.getInteger(POINTS);

            if (_pOneCrash && !_pTwoCrash)
            {
                try
                {
                    int transferPoints = playerOnePoints / _div;

                    playerOneStat.set(POINTS, playerOnePoints - transferPoints);
                    playerOneStat.set(COMP_LOST, playerOneLost + 1);


                    if (Config.DEBUG)
                        _log.info("Olympia Result: "+_playerOneName+" vs "+_playerTwoName+" ... "+_playerOneName+" lost "+transferPoints+" points for crash");						


                    playerTwoStat.set(POINTS, playerTwoPoints + transferPoints);
                    playerTwoStat.set(COMP_WON, playerTwoWon + 1);

                    if (Config.DEBUG)
                        _log.info("Olympia Result: "+_playerOneName+" vs "+_playerTwoName+" ... "+_playerTwoName+" Win "+transferPoints+" points");

                    _sm = new SystemMessage(SystemMessage.S1_HAS_WON_THE_GAME);
                    _sm2 = new SystemMessage(SystemMessage.S1_HAS_GAINED_S2_OLYMPIAD_POINTS);
                    _sm3 = new SystemMessage(SystemMessage.S1_HAS_LOST_S2_OLYMPIAD_POINTS);
                    _sm.addString(_playerTwoName);
                    broadcastMessage(_sm, true);
                    _sm2.addString(_playerTwoName);
                    _sm2.addNumber(transferPoints);
                    broadcastMessage(_sm2, false);
                    _sm3.addString(_playerOneName);
                    _sm3.addNumber(transferPoints);
                    broadcastMessage(_sm3, false);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            else if (_pTwoCrash && !_pOneCrash)
            {
                try
                {

                    int transferPoints = playerTwoPoints / _div;
                    playerTwoStat.set(POINTS, playerTwoPoints - transferPoints);
                    playerTwoStat.set(COMP_LOST, playerTwoLost + 1);

                    if (Config.DEBUG)
                        _log.info("Olympia Result: "+_playerTwoName+" vs "+_playerOneName+" ... "+_playerTwoName+" lost "+transferPoints+" points for crash");

                    playerOneStat.set(POINTS, playerOnePoints + transferPoints);
                    playerOneStat.set(COMP_WON, playerOneWon + 1);

                    if (Config.DEBUG)
                        _log.info("Olympia Result: "+_playerTwoName+" vs "+_playerOneName+" ... "+_playerOneName+" Win "+transferPoints+" points");

                    _sm = new SystemMessage(SystemMessage.S1_HAS_WON_THE_GAME);
                    _sm2 = new SystemMessage(SystemMessage.S1_HAS_GAINED_S2_OLYMPIAD_POINTS);
                    _sm3 = new SystemMessage(SystemMessage.S1_HAS_LOST_S2_OLYMPIAD_POINTS);
                    _sm.addString(_playerOneName);
                    broadcastMessage(_sm, true);
                    _sm2.addString(_playerOneName);
                    _sm2.addNumber(transferPoints);
                    broadcastMessage(_sm2, false);
                    _sm3.addString(_playerTwoName);
                    _sm3.addNumber(transferPoints);
                    broadcastMessage(_sm3, false);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            else if (_pOneCrash && _pTwoCrash)
            {
                try
                {
                    int pointDiff = Math.min(playerOnePoints, playerTwoPoints) / _div;
                    playerOneStat.set(POINTS, playerOnePoints - pointDiff);
                    playerTwoStat.set(POINTS, playerTwoPoints - pointDiff);

                    if (Config.DEBUG)
                        _log.info("Olympia Result: "+_playerOneName+" vs "+_playerTwoName+" ... " + " both lost "+pointDiff+" points for crash");

                    _sm = new SystemMessage(SystemMessage.S1_HAS_LOST_S2_OLYMPIAD_POINTS);
                    _sm.addString(_playerOneName);
                    _sm.addNumber(pointDiff);
                    broadcastMessage(_sm, false);
                    _sm2 = new SystemMessage(SystemMessage.S1_HAS_LOST_S2_OLYMPIAD_POINTS);
                    _sm2.addString(_playerTwoName);
                    _sm2.addNumber(pointDiff);
                    broadcastMessage(_sm2, false);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            playerOneStat.set(COMP_DONE, playerOnePlayed + 1);
            playerTwoStat.set(COMP_DONE, playerTwoPlayed + 1);

            return false;
        }

        return true;
    }

    protected boolean hasWinner()
    {
        if (_aborted || _playerOne == null || _playerTwo == null)
            return true;

        double playerOneHp = 0;

        try
        {
            if (_playerOne != null && _playerOne.getOlympiadGameId() != -1)
                playerOneHp = _playerOne.getCurrentHp();
        }
        catch (Exception e)
        {
            playerOneHp = 0;
        }

        double playerTwoHp = 0;
        try
        {
            if (_playerTwo != null && _playerTwo.getOlympiadGameId()!=-1)
                playerTwoHp = _playerTwo.getCurrentHp();
        }
        catch (Exception e)
        {
            playerTwoHp = 0;
        }

        if (playerTwoHp == 0 || playerOneHp == 0)
            return true;

        return false;
    }

    protected void validateWinner()
    {
        if (_aborted || _playerOne == null || _playerTwo == null || _playerOneDisconnected || _playerTwoDisconnected)
            return;

        StatsSet playerOneStat;
        StatsSet playerTwoStat;

        playerOneStat = Olympiad.getNobleStats(_playerOneID);
        playerTwoStat = Olympiad.getNobleStats(_playerTwoID);

        int _div;
        int _gpreward;

        int playerOnePlayed = playerOneStat.getInteger(COMP_DONE);
        int playerTwoPlayed = playerTwoStat.getInteger(COMP_DONE);
        int playerOneWon = playerOneStat.getInteger(COMP_WON);
        int playerTwoWon = playerTwoStat.getInteger(COMP_WON);
        int playerOneLost = playerOneStat.getInteger(COMP_LOST);
        int playerTwoLost = playerTwoStat.getInteger(COMP_LOST);

        int playerOnePoints = playerOneStat.getInteger(POINTS);
        int playerTwoPoints = playerTwoStat.getInteger(POINTS);

        double playerOneHp = 0;
        try
        {
            if (_playerOne != null && !_playerOneDisconnected)
            {
                if (!_playerOne.isDead())
                    playerOneHp = _playerOne.getCurrentHp()+_playerOne.getCurrentCp();
            }
        }
        catch (Exception e)
        {
            playerOneHp = 0;
        }

        double playerTwoHp = 0;
        try
        {
            if (_playerTwo != null && !_playerTwoDisconnected)
            {
                if (!_playerTwo.isDead())
                    playerTwoHp = _playerTwo.getCurrentHp()+_playerTwo.getCurrentCp();
            }
        }
        catch (Exception e)
        {
            playerTwoHp = 0;
        }

        _sm = new SystemMessage(SystemMessage.S1_HAS_WON_THE_GAME);
        _sm2 = new SystemMessage(SystemMessage.S1_HAS_GAINED_S2_OLYMPIAD_POINTS);
        _sm3 = new SystemMessage(SystemMessage.S1_HAS_LOST_S2_OLYMPIAD_POINTS);

        String result = "";

        // If players crashed, check if they have relogged
        _playerOne =  L2World.getInstance().getPlayer(_playerOneName); 
        _players.set(0, _playerOne);
        _playerTwo =  L2World.getInstance().getPlayer(_playerTwoName);
        _players.set(1, _playerTwo);

        switch (_type)
        {
            case NON_CLASSED:
                _div=5;
                _gpreward = Config.ALT_OLY_NONCLASSED_RITEM_C;
                break;
            default:
                _div=3;
                _gpreward = Config.ALT_OLY_CLASSED_RITEM_C;
                break;
        }

        int pointDiff;

        if (_playerOne == null && _playerTwo == null)
        {
            result=" tie";
            _sm = new SystemMessage(SystemMessage.THE_GAME_ENDED_IN_A_TIE);
            broadcastMessage(_sm, true);
        }
        else if (_playerTwo == null || !_playerTwo.isOnline() || (playerTwoHp == 0 && playerOneHp != 0) 
        	|| (_playerOne != null && _playerOne.dmgDealt > _playerTwo.dmgDealt && playerTwoHp != 0 && playerOneHp != 0))
        {

            pointDiff = playerTwoPoints / _div;
            playerOneStat.set(POINTS, playerOnePoints + pointDiff);
            playerOneStat.set(COMP_WON, playerOneWon + 1);
            playerTwoStat.set(POINTS, playerTwoPoints - pointDiff);
            playerTwoStat.set(COMP_LOST, playerTwoLost + 1);

            _sm.addString(_playerOneName);
            broadcastMessage(_sm, true);
            _sm2.addString(_playerOneName);
            _sm2.addNumber(pointDiff);
            broadcastMessage(_sm2, false);
            _sm3.addString(_playerTwoName);
            _sm3.addNumber(pointDiff);
            broadcastMessage(_sm3, false);

            try
            {
                result=" ("+playerOneHp+"hp vs "+playerTwoHp+"hp - "+_playerOne.dmgDealt+"dmg vs "+_playerTwo.dmgDealt+"dmg) "+_playerOneName+" win "+pointDiff+" points";
                L2ItemInstance item = _playerOne.getInventory().addItem("Olympiad", Config.ALT_OLY_BATTLE_REWARD_ITEM, _gpreward, _playerOne, null);
                InventoryUpdate iu = new InventoryUpdate();
                iu.addModifiedItem(item);
                _playerOne.sendPacket(iu);

                SystemMessage sm = new SystemMessage(SystemMessage.EARNED_S2_S1_s);
                sm.addItemName(item.getItemId());
                sm.addNumber(_gpreward);
                _playerOne.sendPacket(sm);
            }
            catch (Exception e) {}
        }
        else if (_playerOne == null || !_playerOne.isOnline() || (playerOneHp == 0 && playerTwoHp != 0) 
        	|| (_playerTwo != null && _playerTwo.dmgDealt > _playerOne.dmgDealt && playerOneHp != 0 && playerTwoHp != 0))
        {
            pointDiff = playerOnePoints / _div;
            playerTwoStat.set(POINTS, playerTwoPoints + pointDiff);
            playerTwoStat.set(COMP_WON, playerTwoWon + 1);
            playerOneStat.set(POINTS, playerOnePoints - pointDiff);
            playerOneStat.set(COMP_LOST, playerOneLost + 1);

            _sm.addString(_playerTwoName);
            broadcastMessage(_sm, true);
            _sm2.addString(_playerTwoName);
            _sm2.addNumber(pointDiff);
            broadcastMessage(_sm2, false);
            _sm3.addString(_playerOneName);
            _sm3.addNumber(pointDiff);
            broadcastMessage(_sm3, false);

            try
            {
                result=" ("+playerOneHp+"hp vs "+playerTwoHp+"hp - "+_playerOne.dmgDealt+"dmg vs "+_playerTwo.dmgDealt+"dmg) "+_playerTwoName+" win "+pointDiff+" points";
                L2ItemInstance item = _playerTwo.getInventory().addItem("Olympiad", Config.ALT_OLY_BATTLE_REWARD_ITEM, _gpreward, _playerTwo, null);
                InventoryUpdate iu = new InventoryUpdate();
                iu.addModifiedItem(item);
                _playerTwo.sendPacket(iu);

                SystemMessage sm = new SystemMessage(SystemMessage.EARNED_S2_S1_s);
                sm.addItemName(item.getItemId());
                sm.addNumber(_gpreward);
                _playerTwo.sendPacket(sm);
            }
            catch (Exception e) {}
        }

        else
        {
            result =" tie";
            _sm = new SystemMessage(SystemMessage.THE_GAME_ENDED_IN_A_TIE);

            broadcastMessage(_sm, true);
            int pointOneDiff = playerOnePoints / _div;
            int pointTwoDiff = playerTwoPoints / _div;

            playerOneStat.set(POINTS, playerOnePoints - pointOneDiff);
            playerOneStat.set(COMP_LOST, playerOneLost + 1);

            playerTwoStat.set(POINTS, playerTwoPoints - pointTwoDiff);
            playerTwoStat.set(COMP_LOST, playerTwoLost + 1);

            _sm2 = new SystemMessage(SystemMessage.S1_HAS_LOST_S2_OLYMPIAD_POINTS);
            _sm2.addString(_playerOneName);
            _sm2.addNumber(pointOneDiff);
            broadcastMessage(_sm2, false);
            _sm3 = new SystemMessage(SystemMessage.S1_HAS_LOST_S2_OLYMPIAD_POINTS);
            _sm3.addString(_playerTwoName);
            _sm3.addNumber(pointTwoDiff);
            broadcastMessage(_sm3, false);
        }

        if (Config.DEBUG)
            _log.info("Olympia Result: "+_playerOneName+" vs "+_playerTwoName+" ... "+result);

        playerOneStat.set(COMP_DONE, playerOnePlayed + 1);
        playerTwoStat.set(COMP_DONE, playerTwoPlayed + 1);

        for (int i = 40; i > 10; i -= 10)
        {
            _sm = new SystemMessage(SystemMessage.YOU_WILL_GO_BACK_TO_THE_VILLAGE_IN_S1_SECOND_S);
            _sm.addNumber(i);
            broadcastMessage(_sm, false);
            try
            {
                Thread.sleep(10000);
            }
            catch (InterruptedException e) {}

            if (i == 20)
            {
                _sm = new SystemMessage(SystemMessage.YOU_WILL_GO_BACK_TO_THE_VILLAGE_IN_S1_SECOND_S);
                _sm.addNumber(10);
                broadcastMessage(_sm, false);
                try
                {
                    Thread.sleep(5000);
                }
                catch (InterruptedException e) {}
            }
        }

        for (int i = 5; i > 0; i--)
        {
            _sm = new SystemMessage(SystemMessage.YOU_WILL_GO_BACK_TO_THE_VILLAGE_IN_S1_SECOND_S);
            _sm.addNumber(i);
            broadcastMessage(_sm, false);
	
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {}
        }
    }

    protected void additions()
    {
        for (L2PcInstance player : _players)
        {
            try
            {
                // Wind Walk Buff for both
                L2Skill skill;
                SystemMessage sm;

                skill = SkillTable.getInstance().getInfo(1204, 2);
                skill.getEffects(player, player);

                sm = new SystemMessage(SystemMessage.YOU_FEEL_S1_EFFECT);
                sm.addSkillName(skill);
                player.sendPacket(sm);

                if (player.isMageClass())
                {
                    // Acumen Buff to mystics
                    skill = SkillTable.getInstance().getInfo(1085, 1);
                    skill.getEffects(player, player);

                    sm = new SystemMessage(SystemMessage.YOU_FEEL_S1_EFFECT);
                    sm.addSkillName(skill);

                }
                else
                {
                    // Haste Buff to fighters
                    skill = SkillTable.getInstance().getInfo(1086, 1);
                    skill.getEffects(player, player);

                    sm = new SystemMessage(SystemMessage.YOU_FEEL_S1_EFFECT);
                    sm.addSkillName(skill);
                }
                player.sendPacket(sm);

            }
            catch (Exception e) {}
            finally
            {
                player.dmgDealt = 0;
            }
        }
    }

    protected boolean makeCompetitionStart()
    {
        if (_aborted)
            return false;

        _sm = new SystemMessage(SystemMessage.STARTS_THE_GAME);
        broadcastMessage(_sm, true);

        try
        {
        	_state = PLAYING;

            for (L2PcInstance player : _players)
            {
                if (player == null)
                    continue;

                player.setIsOlympiadStart(true);
                // Send user status to opponent
                if (player == _playerOne)
                    _playerTwo.sendPacket(new ExOlympiadUserInfo(player, 1));
                if (player == _playerTwo)
                    _playerOne.sendPacket(new ExOlympiadUserInfo(player, 1));
            }
            
            // Broadcast users info to spectators
            try
            {
	            for (L2PcInstance spectator : OlympiadManager.STADIUMS[_stadiumID].getSpectators())
	            {
	            	Olympiad.broadcastUsersInfo(spectator);
	            }
            }
            catch (Exception e)
            {
            	// In this case, do not do anything
            }
        }
        catch (Exception e)
        { 
            _aborted = true;
            return false; 
        }
        return true;
    }

    protected L2PcInstance[] getPlayers()
    {
        if (_playerOne == null || _playerTwo == null)
            return null;

        L2PcInstance[] players = new L2PcInstance[2];
        players[0] = _playerOne;
        players[1] = _playerTwo;

        return players;
    }
    
    protected int getStadiumID()
    {
    	return _stadiumID;
    }
    
    protected byte getState()
    {
    	return _state;
    }
    
    protected void setState(byte value)
    {
    	_state = value;
    }
    
    protected String getTitle()
	{
		return _playerOneName + " / " + _playerTwoName;
	}
}

/**
 *
 * @author ascharot
 * 
 */
class OlympiadGameTask implements Runnable
{
	protected static final Logger _log = Logger.getLogger(OlympiadGameTask.class.getName());

	protected OlympiadGame _game = null;
	
	protected static final long BATTLE_PERIOD = Config.ALT_OLY_BATTLE; // Usually several minutes

   	private boolean _terminated = false;
   	private boolean _started = false;

   	public OlympiadGameTask(OlympiadGame game)
   	{
       	_game = game;
   	}

   	protected void cleanGame()
   	{
       	_started = false;
       	_terminated = true;

       	if (_game.getState() > OlympiadGame.INITIAL)
       	{
    	   	_game.playersStatusBack();
           	_game.removals();
           	_game.portPlayersBack();
       	}

       	_game.setState(OlympiadGame.INITIAL);

       	// Notify spectators that match ended
       	try
       	{
       		List<L2PcInstance> spectators = OlympiadManager.STADIUMS[_game.getStadiumID()].getSpectators();
	       	if (spectators != null)
			{
				for (L2PcInstance spec : spectators)
				{
					if (spec != null)
						spec.sendPacket(new ExOlympiadMatchEnd());
				}
			}
       	}
       	catch (Exception e)
       	{
       		// Do not do anything
       	}
       	
       	_game.clearPlayers();
       	OlympiadManager.getInstance().removeGame(_game);
       	_game = null;
   	}
   	
   	public boolean isTerminated()
   	{
	   	return _terminated || _game._aborted;
   	}

   	public boolean isStarted()
   	{
   		return _started;
   	}

   	@Override
   	public void run()
   	{
   		_started = true;
   		if (_game != null)
   		{
   			if (_game._playerOne != null && _game._playerTwo != null)
   			{
   				// Waiting to teleport to arena
   				for (int i = 120; i > 10; i -= 5)
   				{
   					switch(i)
   					{
   						case 120:
   						case 60:
   						case 30:
   						case 15: 
   							_game.sendMessageToPlayers(false, i);
   							break;
   					}

   					try
   					{
   						Thread.sleep(5000);
   					}
   					catch (InterruptedException e){}
   				}

   				for (int i = 5; i > 0; i--)
   				{
   					_game.sendMessageToPlayers(false,i);		    			
   					try
   					{
   						Thread.sleep(1000);
   					}
   					catch (InterruptedException e){}
   				}

   				// Check if players are qualified to fight
   				if (!_game._playerOne.checkOlympiadConditions())
   					_game._playerOne = null;
   				if (!_game._playerTwo.checkOlympiadConditions())
   					_game._playerTwo = null;

   				// Checking for opponents and teleporting to arena
   				if (!_game.checkBattleStatus())
   				{
   					cleanGame();
   					return;
   				}

   				_game.portPlayersToArena();
   				_game.removals();

   				try
   				{
   					Thread.sleep(5000);
   				}
   				catch (InterruptedException e){}

   				synchronized(this)
   				{
   					if (!OlympiadGame._battleStarted)
   						OlympiadGame._battleStarted = true;
   				}

   				for (int i = 60; i > 10; i -= 10)
   				{
   					_game.sendMessageToPlayers(true, i);
   					try
                  	{
   						Thread.sleep(10000);
                  	}
   					catch (InterruptedException e){}

   					if (i == 20)
   					{
   						_game.additions();
   						_game.sendMessageToPlayers(true,10);
   						try
   						{
   							Thread.sleep(5000);
   						}
   						catch (InterruptedException e){}
   					}
   				}

   				for (int i = 5; i > 0; i--)
   				{
   					_game.sendMessageToPlayers(true,i);
   					try
   					{
   						Thread.sleep(1000);
   					}
   					catch (InterruptedException e){}
   				}

   				if (!_game.checkBattleStatus())
   				{
   					cleanGame();
   					return;
   				}

   				_game.makeCompetitionStart();

   				// Wait several minutes (Battle)
   				for (int i = 0; i < BATTLE_PERIOD; i += 10000)
   				{
   					try
   					{
   						Thread.sleep(10000);
   						// If the game has Winner then stop waiting battle_period and validate winner
   						if (_game.hasWinner())
   							break;

   						if (!_game.checkBattleStatus())
   						{
   							cleanGame();
   							return;
   						}
   					}
   					catch (InterruptedException e){}
   				}

   				_game.validateWinner();
   				cleanGame();
            }
        }
    }
}
