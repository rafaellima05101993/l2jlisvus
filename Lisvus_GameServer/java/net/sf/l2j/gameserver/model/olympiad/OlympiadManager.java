/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.model.olympiad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.model.actor.instance.L2PcInstance;
import net.sf.l2j.gameserver.model.olympiad.Olympiad.COMP_TYPE;
import net.sf.l2j.util.Rnd;

/**
 * @author DnR
 *
 */
class OlympiadManager implements Runnable
{
    private Map<Integer, OlympiadGame> _olympiadInstances;
    
    protected static final OlympiadStadium[] STADIUMS = 
    {
        new OlympiadStadium(-20814, -21189, -3030),
        new OlympiadStadium(-120324, -225077, -3331),
        new OlympiadStadium(-102495, -209023, -3331),
        new OlympiadStadium(-120156, -207378, -3331),
        new OlympiadStadium(-87628, -225021, -3331),
        new OlympiadStadium(-81705, -213209, -3331),
        new OlympiadStadium(-87593, -207339, -3331),
        new OlympiadStadium(-93709, -218304, -3331),
        new OlympiadStadium(-77157, -218608, -3331),
        new OlympiadStadium(-69682, -209027, -3331),
        new OlympiadStadium(-76887, -201256, -3331),
        new OlympiadStadium(-109985, -218701, -3331),
        new OlympiadStadium(-126367, -218228, -3331),
        new OlympiadStadium(-109629, -201292, -3331),
        new OlympiadStadium(-87523, -240169, -3331),
        new OlympiadStadium(-81748, -245950, -3331),
        new OlympiadStadium(-77123, -251473, -3331),
        new OlympiadStadium(-69778, -241801, -3331),
        new OlympiadStadium(-76754, -234014, -3331),
        new OlympiadStadium(-93742, -251032, -3331),
        new OlympiadStadium(-87466, -257752, -3331),
        new OlympiadStadium(-114413, -213241, -3331)
    };

    private static OlympiadManager _instance;
    
    public static OlympiadManager getInstance()
	{
		if (_instance == null)
		{
			_instance = new OlympiadManager();
		}
		return _instance;
	}
    
	private OlympiadManager()
	{
        _olympiadInstances = new HashMap<>();
	}

    @Override
	public synchronized void run()
    {
        if (Olympiad.getInstance().isOlympiadEnd())
            return;

        Map<Integer, OlympiadGameTask> _gamesQueue = new HashMap<>();
        while (Olympiad.getInstance().getCompPeriodState() != Olympiad.NONE)
        {
            if (Olympiad.getNobleCount() == 0)
            {
                try
                {
                    wait(60000);
                }
                catch(InterruptedException e)
                {
                    return;
                }
                continue;
            }

            int _gamesQueueSize = 0;

            int classBasedPgCount = 0;
            for (List<L2PcInstance> classList : Olympiad.getRegisteredClassBased().values())
                classBasedPgCount += classList.size();

            if (classBasedPgCount >= Config.ALT_OLY_CLASSED || Olympiad.getRegisteredNonClassBased().size() >= Config.ALT_OLY_NONCLASSED)
            {
                // Set up the games queue   				
	            for (int i = 0; i < STADIUMS.length; i++)
                {
	                if (!existNextOpponents(Olympiad.getRegisteredNonClassBased()) && !existNextOpponents(getRandomClassList(Olympiad.getRegisteredClassBased())))
                        break;

                    if (STADIUMS[i].isFreeToUse())
                    {
                        if (existNextOpponents(Olympiad.getRegisteredNonClassBased()))
                        {
                            try
                            {
                                _olympiadInstances.put(i, new OlympiadGame(i, COMP_TYPE.NON_CLASSED, nextOpponents(Olympiad.getRegisteredNonClassBased()), STADIUMS[i].getCoordinates()));
                                _gamesQueue.put(i, new OlympiadGameTask(_olympiadInstances.get(i)));
                                STADIUMS[i].setStadiaBusy();
                            }
                            catch(Exception e)
                            {
                                if (_olympiadInstances.get(i) != null)
                                {
                                    for (L2PcInstance player : _olympiadInstances.get(i).getPlayers())
                                    {
                                        player.sendMessage("Your olympiad registration was cancelled due to an error.");
                                        player.setIsInOlympiadMode(false);
                                        player.setIsOlympiadStart(false);
                                        player.setOlympiadSide(-1);
                                        player.setOlympiadGameId(-1);
                                    }
                                    _olympiadInstances.remove(i);
                                }

                                if (_gamesQueue.get(i) != null)
                                    _gamesQueue.remove(i);
                                STADIUMS[i].setStadiaFree();

                                // Try to reuse this stadium next time
                                i--;
                            }
                        }
                        else if (existNextOpponents(getRandomClassList(Olympiad.getRegisteredClassBased())))
                        {
                            try
                            {
                                _olympiadInstances.put(i, new OlympiadGame(i, COMP_TYPE.CLASSED, nextOpponents(getRandomClassList(Olympiad.getRegisteredClassBased())), STADIUMS[i].getCoordinates()));
                                _gamesQueue.put(i,new OlympiadGameTask(_olympiadInstances.get(i)));
                                STADIUMS[i].setStadiaBusy();
                            }
                            catch(Exception e)
                            {
                                if (_olympiadInstances.get(i) != null)
                                {
                                    for (L2PcInstance player : _olympiadInstances.get(i).getPlayers())
                                    {
                                        player.sendMessage("Your olympiad registration was cancelled due to an error.");
                                        player.setIsInOlympiadMode(false);
                                        player.setIsOlympiadStart(false);
                                        player.setOlympiadSide(-1);
                                        player.setOlympiadGameId(-1);
                                    }
                                    _olympiadInstances.remove(i);
                                }

                                if (_gamesQueue.get(i)!= null)
                                    _gamesQueue.remove(i);
                                STADIUMS[i].setStadiaFree();

                                // Try to reuse this stadium next time
                                i--;
                            }
                        }
                    }
                    else
                    {
                        if (_gamesQueue.get(i) == null || _gamesQueue.get(i).isTerminated() || _gamesQueue.get(i)._game == null)
                        {
                            try
                            {
                                _olympiadInstances.remove(i);
                                _gamesQueue.remove(i);
                                STADIUMS[i].setStadiaFree();
                                i--;
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                // Start games
                _gamesQueueSize = _gamesQueue.size();
                for (int i = 0; i < _gamesQueueSize; i++)
                {
                    if (_gamesQueue.get(i) != null && !_gamesQueue.get(i).isTerminated() && !_gamesQueue.get(i).isStarted())
                    {
                        // start new games
                        Thread T = new Thread(_gamesQueue.get(i));
                        T.start();
                    }
                }
            }

            // Wait 30 sec due to server stress
            try
            {
                wait(30000);
            }
            catch (InterruptedException e)
            {
                return;
            }
        }

        // When competition time finishes wait for all games to be terminated before executing the cleanup code
        boolean allGamesTerminated = false;
        // Wait for all games to be terminated
        while (!allGamesTerminated)
        {
            try
            {
                wait(30000);
            }
            catch (InterruptedException e)
            {
            }

            if (_gamesQueue.size() == 0)
                allGamesTerminated = true;
            else
            {
                for (OlympiadGameTask game : _gamesQueue.values())
                    allGamesTerminated = allGamesTerminated || game.isTerminated();
            }
        }

        // When all games are terminated clean up everything
        _gamesQueue.clear();

        // Wait 20 seconds
        _olympiadInstances.clear();
        Olympiad.clearRegistered();

		OlympiadGame._battleStarted = false;
	}
    
    protected OlympiadGame getOlympiadGame(int index)
	{
		if (!_olympiadInstances.isEmpty())
		{
			return _olympiadInstances.get(index);
		}
		return null;
	}

    protected void removeGame(OlympiadGame game)
 	{
        if (!_olympiadInstances.isEmpty())
        {
            for (int i = 0; i < _olympiadInstances.size(); i++)
            {
                if (_olympiadInstances.get(i) == game)
                    _olympiadInstances.remove(i);
            }
        }
    }
    
    protected Map<Integer, OlympiadGame> getOlympiadGames()
    {
        return _olympiadInstances;
    }

    private List<L2PcInstance> getRandomClassList(Map<Integer, List<L2PcInstance>> list)
    {
        if (list.size() == 0)
            return null;

        Map<Integer, List<L2PcInstance>> tmp = new HashMap<>();
        int tmpIndex = 0;
        for (List<L2PcInstance> l : list.values())
        {
            tmp.put(tmpIndex, l);
            tmpIndex ++;
        }

        List<L2PcInstance> rndList = new ArrayList<>();
        int classIndex = 0;

        if (tmp.size() == 1)
            classIndex = 0;
        else
            classIndex = Rnd.nextInt(tmp.size());
        rndList = tmp.get(classIndex);
        return rndList;
    }

    private List<L2PcInstance> nextOpponents(List<L2PcInstance> list)
    {
        List<L2PcInstance> opponents = new ArrayList<>();
        if (list.size() == 0)
            return opponents;

        int loopCount = (list.size() / 2);
        int first;
        int second;

        if (loopCount < 1)
            return opponents;

        first = Rnd.nextInt(list.size());
        opponents.add(list.get(first));
        list.remove(first);

        second = Rnd.nextInt(list.size());
        opponents.add(list.get(second));
        list.remove(second);

        return opponents;
    }

    private boolean existNextOpponents(List<L2PcInstance> list)
    {
        if (list == null)
            return false;

        if (list.size() == 0)
            return false;

        int loopCount = list.size() >> 1;
        if (loopCount < 1)
            return false;

        return true;
    }

    protected Map<Integer, String> getAllTitles()
	{
		Map<Integer, String> titles = new HashMap<>();
		for (OlympiadGame instance : _olympiadInstances.values())
		{
			if (instance.getState() == OlympiadGame.INITIAL)
				continue;
			
			titles.put(instance.getStadiumID(), instance.getTitle());
		}
		
		return titles;
	}
}
