/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author DnR
 * 
 * This class is important for loading all scripts written in Java.
 */
public class JavaScriptManager
{
	public static void main(String args[])
	{
		// AI Section
		new ai.group_template.AntNest();
		new ai.group_template.DevilsIsle();
		new ai.group_template.FeedableBeasts();
		new ai.group_template.FleeingNpc();
		new ai.group_template.ForgeOfTheGods();
		new ai.group_template.FrenzyOnAttack();
		new ai.group_template.OutlawForest();
		new ai.group_template.PlainsOfDion();
		new ai.group_template.SearchingMaster();
		new ai.group_template.TowerOfInsolence();
		new ai.group_template.TreasureChests();
		new ai.group_template.ValleyOfSaints();

		new ai.individual.Antharas();
		new ai.individual.Baium();
		new ai.individual.Core();
		new ai.individual.FallenOrcShaman();
		new ai.individual.Grandis();
		new ai.individual.OlMahumGeneral();
		new ai.individual.Orfen();
		new ai.individual.TimakOrcTroopLeader();
		new ai.individual.QueenAnt();
		new ai.individual.Valakas();
		new ai.individual.Zaken();

		new ai.quest.CatsEyeBandit();
		new ai.quest.FairyTrees();
		new ai.quest.HoneyBear();
		new ai.quest.Kirunak();
		new ai.quest.Merkenis();
		new ai.quest.Nerkas();

		// Custom
		new custom.EchoCrystals.EchoCrystals();
		new custom.HeroItems.HeroItems();
		new custom.KetraOrcSupport.KetraOrcSupport();
		new custom.NpcLocationInfo.NpcLocationInfo();
		new custom.VarkaSilenosSupport.VarkaSilenosSupport();

		// Teleports
		new teleports.RaceTrack.RaceTrack();
		new teleports.TeleportWithCharm.TeleportWithCharm();
		new teleports.ToIVortex.ToIVortex();
		
		// Quests
		new quests.Q001_LettersOfLove.Q001_LettersOfLove();
	}
}
